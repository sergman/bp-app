import React from 'react';
import './Form.css';
import Button from '../Button/Button';
import InputField from '../InputField/InputField';


const Form = ({fieldNames}) => (
  <div>
    <form>
      {fieldNames.map(fieldName=>(
        <InputField key={fieldName.id} fieldName={fieldName.fieldName} type={fieldName.type}/>))}
    </form>
    <div className="controlBtn">
      <Button btnName="Clear"/>
      <Button btnName="Autoscan"/>
    </div>
  </div>
)   

export default Form;
