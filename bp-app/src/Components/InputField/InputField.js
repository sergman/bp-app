import React from 'react';
import './InputField.css';

const InputField = ({fieldName, type}) => (
    <li><input type={type} placeholder={fieldName}></input></li>
  )   

export default InputField;