import React from 'react';
import './Button.css';

const Button = ({btnName}) => (
    <button>{btnName}</button>
  )   

export default Button;
