import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Form from './Components/Form/Form';


const fieldNamesList = [
    {id:1, fieldName:"SYS", type:"number"},
    {id:2, fieldName:"DIA", type:"number"},
    {id:3, fieldName:"Pulse", type:"number"},
    {id:4, fieldName:"Date", type:"date"},
    {id:5, fieldName:"Submit", type:"submit"},
  ]
  
ReactDOM.render(
    <Form fieldNames={fieldNamesList}/>,
    document.getElementById("root")
)

